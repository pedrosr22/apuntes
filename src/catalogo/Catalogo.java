/**
Copyright [año] [nombre del propietario del copyright]
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
**/

package catalogo; 

import java.util.ArrayList;
import java.io.IOException;
import java.io.File;
import java.util.Scanner;
import java.io.FileWriter;

/**
* Esta clase es la responsable del añadido de contactos y generación del listado de contactos. 
 **/

public class Catalogo{
	private String nombre;
	private ArrayList<Producto> catalogodedj = new ArrayList <> ();  

/**
*Devuelve string de la forma de nombre.
*@return nombre del producto 
**/
	public String getNombre(){
	   return this.nombre; }
/** 

* Añade el producto la libreta.
*@param producto El producto a añadir
**/
	public void annadir (Producto producto){
		catalogodedj.add(producto);
		escribirProductos();
	}

/**
*Borra los productos del catálogo
*@param producto El producto a borrar
**/

	public void borrar (Producto producto){
		catalogodedj.remove(producto);
		escribirProductos();

	}


/**
*Constructor de catálogo. 
**/

    public Catalogo (){
        cargarProductos();
  }

/**
*Constructor de catálogo con nombre.
* @param nombre Nombre del catálogo
**/
  public Catalogo (String nombre){
  	this.nombre = nombre;
  }


/** 
* Redefinición del metodo toString en forma de lista con el array
* Se mostrará cada producto añadido y sus atributos.
**/ 
	@Override
	public String toString(){
		StringBuilder textoProductos = new StringBuilder();
		for (Producto producto: catalogodedj) textoProductos.append(producto);
			return textoProductos.toString(); 
	}

	//cargar Productos (de lo q habia antes al array)

/** 

* Carga los productos del fichero al ArrayList.
* Utilizamos .createNewFile
**/
	public void cargarProductos (){
		try{

			File fichero = new File ("mesasdj.txt");
			fichero.createNewFile(); //crea un fichero si no existe
			//escaneamos para estructurarlo bien 
			Scanner sc = new Scanner (fichero);
			sc.useDelimiter(",|\n");
			while (sc.hasNext()){
				catalogodedj.add(new Producto (sc.next() , sc.next(), sc.next(), Integer.parseInt(sc.next()) ));
				//String, String, String, int 

			}

		}catch(IOException ex){
		System.err.println( "Error en la carga de Productos al sistema");}
	}

/** 

* Escribe los productos del ArrayList al fichero.
* Utilizamos FileWriter
**/
	public void escribirProductos (){
		try{

			FileWriter fw = new FileWriter("mesasdj.txt");
			fw.write (toString().trim());
			fw.close();
			

		}catch(IOException ex){
			System.err.println("Error al escribir los Productos");
		}
	}

/** 

* Genera una hoja de cálculo a partir del catálogo.
* Tiene las columnas nombre , marca, color e identificador.
**/

 public void generarHojaCalc(){
		try{

			FileWriter fw = new FileWriter("mesasdj.csv");
			fw.write("nombre, marca, color, identificador\n");
			fw.write (toString().trim());
			fw.close();
			

		}catch(IOException ex){
			System.err.println("Error al generar hoja de cálculo");
		}
	}



}
