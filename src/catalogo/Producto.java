/**
Copyright [año] [nombre del propietario del copyright]
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
**/

package catalogo;

/**
* Esta clase es la responsable de establecer los atributos del objeto producto. 
 **/

public class Producto{
	private String nombre;
	private String marca;
	private int id;
	private String color; 

	//no ponemos set, porq ya lo vamos a poner como get en java -jar catalogo.jar..

/**
*Devuelve string de la forma de nombre.
*@return nombre del producto 
**/	
	public String getNombre(){
		return this.nombre;
	}

	/**
*Devuelve string de la forma de marca.
*@return marca del producto 
**/	
	public String getMarca(){
		return marca;
	}

/**
*Devuelve string de la forma de color.
*@return color del producto 
**/	

	public String getColor(){
		return color;
	}
	/**
*Devuelve un int  de la forma de id.
*@return precio del producto 
**/	

	public int getId(){
		return id;
	}

//constructor:
	/**
	*Constructor de la clase producto con todos sus atributos.
	*@param nombreinput nombre del producto.
	*@param marcaimput marca del producto.
	*@param colorinput color del producto.
	*@param idinput identificador del producto.
	**/

	public Producto (String nombreinput, String marcaimput, String colorinput, int idinput){
		this.nombre = nombreinput;
		this.marca = marcaimput;
		this.color = colorinput;
		this.id = idinput;

	}
/**
*Constructor solo con el id.
*@param idinput identificador del producto.
**/
	public Producto (int idinput){
		this.id = idinput; }

/**
*Devuelve una cadena de caracteres de la forma nombre , marca, color, precio.
*@return cadena de caracateres con el nombre, la marca,el color y el precio del producto.
**/	
	@Override
	public String toString() {
		return getNombre() + "," + getMarca() + ","  + getColor()+ "," + getId() + "\n";
	}

/**
*Método Equals redefinido.
*Establece que dos productos son iguales si tienen el mismo id.
**/

@Override
	public boolean equals(Object obj){
		Producto producto = (Producto) obj;
		return this.getId() == producto.getId();	
	}





}