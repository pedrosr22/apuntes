/**
Copyright [año] [nombre del propietario del copyright]
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
**/
package interfaz;

import catalogo.*;
import java.io.IOException;
import java.io.File;
import java.io.FileWriter;
/**
* Esta clase es la responsable imprimir las funciones básicas del catálogo.
* Se pueden imprimir el propio catálogo, añadir productos al catálogo y mostrar la ayuda. 
 **/

public class Interfaz{
    private Catalogo catalogo; 

    private static String negrita = "\033[1m"; 
    private static String normal = "\033[0m"; 
    private static String subrayado = "\033[4m"; 

    private static String rojo = "\u001B[31m";
    private static String negro = "\u001B[30m";

/**
* Este método muestra una pequeña guía para utilizar el programa 
 **/
    private static void mostrarAyuda(){
        System.out.println(negrita + "NOMBRE" + normal);
        System.out.println("\t" + subrayado + "Catálogo de mesas de dj" + normal);
        System.out.println(negrita + "\nSYNOPSIS" + normal);
        System.out.println("\tjava -jar catalogo.jar" + subrayado + "[annadir <nombre> <marca> <color> <identificador>| mostrar | ayuda | csv | borrar <identificador>] | modificar <identificador> <nombre> <marca> <color>" + normal);
        System.out.println(negrita + "\nDESCRIPCIÓN" + normal);
        System.out.println("\tGestiona un catálogo de controladoras de dj. Permite añadir, borrar, modificar, mostrar los Productos y generar una hoja de cálculo.");
        System.out.println(negrita + "\nEJEMPLOS" + normal);
        System.out.println("\tEjemplo 1. Añadido de un producto:\n");
        System.out.println(negrita + "\t\tjava -jar catalogo.jar annadir DjInpulse500 hercules gold 1\n" + normal);
        System.out.println("\tEjemplo 2. Borrado de un producto:\n");
        System.out.println(negrita + "\t\tjava -jar catalogo.jar borrar 1\n" + normal);
        System.out.println("\tEjemplo 3. Listado de contactos:\n");
        System.out.println(negrita + "\t\tjava -jar catalogo.jar mostrar" + normal);
        System.out.println("\t\nEjemplo 4. Muestra esta ayuda:\n");
        System.out.println(negrita + "\t\tjava -jar catalogo.jar ayuda\n" + normal);
        System.out.println("\t\nEjemplo 5. modificar un producto:\n");
        System.out.println(negrita + "\t\tjava -jar catalogo.jar modificar 1 DjInpulse500 hercules verde \n" + normal);
        System.out.println("\t\nEjemplo 6. Generar Hoja de Cálculo:\n");
        System.out.println(negrita + "\t\tjava -jar catalogo.jar csv\n" + normal);
    }

 
    /**
* La función ejecutar se encarga de entender lo indicado por consola e imprimirlo.
*Imprime las cuatro funciones básicas: añadir producto, borrar producto, mostrar catálogo y mostrar ayuda.
*@param instruccion instruccion o palabras despues de 'java -jar catalogo.repo'
 **/

    public static void ejecutar(String[] instruccion){
        Catalogo catalogo = new Catalogo();
        if (instruccion.length == 0) mostrarAyuda();
        else if (instruccion[0].equalsIgnoreCase("annadir") && instruccion.length == 5){
            catalogo.annadir(new Producto(instruccion[1], instruccion[2], instruccion[3], Integer.parseInt(instruccion[4]))); 
        }else if (instruccion[0].equalsIgnoreCase("borrar") && instruccion.length == 2){
           Producto productoborrar = new Producto(Integer.parseInt(instruccion[1])); 
           catalogo.borrar(productoborrar); }
           // creo un nuevo producto con un atributo (acuerdate de crear el constructor)
           // 1. crea un producto 
           // 2. Ese producto tiene el identificador q has añadido en terminal
           // 3. Como equals--> =id <--> =producto >> borrar productoborrar

        else if (instruccion[0].equalsIgnoreCase("modificar") && instruccion.length == 5){
           Producto productoborrar = new Producto(Integer.parseInt(instruccion[1])); 
           catalogo.borrar(productoborrar);
           catalogo.annadir(new Producto(instruccion[2], instruccion[3], instruccion[4], Integer.parseInt(instruccion[1])));
            }
        else if (instruccion[0].equalsIgnoreCase("mostrar") && instruccion.length == 1) System.out.println(catalogo);
        else if (instruccion[0].equalsIgnoreCase("ayuda") && instruccion.length == 1) mostrarAyuda();
        else if (instruccion[0].equalsIgnoreCase("csv") && instruccion.length == 1) catalogo.generarHojaCalc();
        else{
            System.out.println(rojo + "Error en la instrucción" + normal); 
            mostrarAyuda();
        }

}
    
}
